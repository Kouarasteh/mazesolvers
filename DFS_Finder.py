from Maze_Importer import MazeImporter
import time
class DFSFinder:
    def __init__(self, mi):
        self.mi = mi
        mazeH = len(self.mi.lines)
        self.startPos = self.findStart()
        self.endPos = self.findEnd()
        self.dims = (len(self.mi.lines[0]), len(self.mi.lines))
        self.isDiscovered = [[0 for x in range(self.dims[0])] for y in range(self.dims[1])]
        self.myParent = [[(-1,-1) for x in range(self.dims[0])] for y in range(self.dims[1])]
        self.numExpanded = 0
        self.numVisited = 0
    def printMaze(self):
        for line in self.mi.lines:
            print(''.join(line))

    def printDiscovered(self):
        for idx in range(self.dims[1]):
            for idx2 in range(self.dims[0]):
                if(self.mi.lines[idx][idx2] == '%'):
                    self.isDiscovered[idx][idx2] = '%'
                print(self.isDiscovered[idx][idx2], end='')
            print()

    def findStart(self):
        for idx in range(len(self.mi.lines)):
            for idx2 in range(len(self.mi.lines[idx])):
                if(self.mi.lines[idx][idx2] == 'P'):
                    startx = idx2
                    starty = idx
        return (startx,starty)

    def findEnd(self):
        for idx in range(len(self.mi.lines)):
            for idx2 in range(len(self.mi.lines[idx])):
                if(self.mi.lines[idx][idx2] == '.'):
                    endx = idx2
                    endy = idx
        return (endx,endy)

    def DFS(self):
        stack = []
        stack.append(self.startPos)
        while(len(stack) != 0):
            curr = stack.pop()
            self.numVisited+=1
            if(self.mi.lines[curr[1]][curr[0]] == '.'):
                return
            if(self.isDiscovered[curr[1]][curr[0]] != 1):
                self.isDiscovered[curr[1]][curr[0]] = 1
                nbs = self.getMyValidNeighbors(curr)
                if(len(nbs) > 0):
                    self.numExpanded+=1
                for n in nbs:
                    if(self.isDiscovered[n[1]][n[0]] == 0):
                        stack.append(n)
                        self.myParent[n[1]][n[0]] = curr

    def getMyValidNeighbors(self, pos):
        myX = pos[0]
        myY = pos[1]
        nbs = []
        if(self.mi.lines[myY-1][myX] != '%'):
            nbs.append((myX,myY-1))
        if(self.mi.lines[myY][myX+1] != '%'):
            nbs.append((myX+1,myY))
        if(self.mi.lines[myY+1][myX] != '%'):
            nbs.append((myX,myY+1))
        if(self.mi.lines[myY][myX-1] != '%'):
            nbs.append((myX-1,myY))
        return nbs

    def printSolution(self):
        self.solution = self.isDiscovered
        self.path = []
        currLoc = self.endPos
        self.path.append(currLoc)
        while(currLoc != self.startPos):
            nextLoc = self.myParent[currLoc[1]][currLoc[0]]
            self.path.append(nextLoc)
            currLoc = nextLoc

        for item in self.path:
            self.solution[item[1]][item[0]] = '.'
        for idx in range(self.dims[1]):
            for idx2 in range(self.dims[0]):
                if(self.solution[idx][idx2] == 0 or self.solution[idx][idx2] == 1):
                    self.solution[idx][idx2] = ' '
        self.solution[self.startPos[1]][self.startPos[0]] = 'P'
        for i in range(self.dims[1]):
            for i2 in range(self.dims[0]):
                print(self.solution[i][i2], end='')
            print()

start_time = time.time()
myMazeImp = MazeImporter('bigMaze.txt')
myDFS = DFSFinder(myMazeImp)
myDFS.DFS()
end_time = time.time()
myDFS.printDiscovered()
print()
myDFS.printSolution()
print("--- %s seconds ---" % (end_time - start_time))
print("--- %s path cost ---" % (len(myDFS.path)-1))
print("--- %s nodes expanded ---" % myDFS.numExpanded)

start_time = time.time()
myMazeImp = MazeImporter('mediumMaze.txt')
myDFS = DFSFinder(myMazeImp)
myDFS.DFS()
end_time = time.time()
myDFS.printDiscovered()
print()
myDFS.printSolution()
print("--- %s seconds ---" % (end_time - start_time))
print("--- %s path cost ---" % (len(myDFS.path)-1))
print("--- %s nodes expanded ---" % myDFS.numExpanded)


start_time = time.time()
myMazeImp = MazeImporter('openMaze.txt')
myDFS = DFSFinder(myMazeImp)
myDFS.DFS()
end_time = time.time()
myDFS.printDiscovered()
print()
myDFS.printSolution()
print("--- %s seconds ---" % (end_time - start_time))
print("--- %s path cost ---" % (len(myDFS.path)-1))
print("--- %s nodes expanded ---" % myDFS.numExpanded)
