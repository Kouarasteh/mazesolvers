from Maze_Importer import MazeImporter
import time
import queue
class ASTARFinder:
    def __init__(self, mi):
        self.mi = mi
        self.startPos = self.findStart()
        self.endPos = self.findEnd()
        self.dims = (len(self.mi.lines[0]), len(self.mi.lines))
        self.isDiscovered = [[0 for x in range(self.dims[0])] for y in range(self.dims[1])]
        self.myParent = [[(-1,-1) for x in range(self.dims[0])] for y in range(self.dims[1])]
        self.numExpanded = 0
        self.path_cost = [[(0) for x in range(self.dims[0])] for y in range(self.dims[1])]

    def printMaze(self):
        for line in self.mi.lines:
            print(''.join(line))

    def findStart(self):
        for idx in range(len(self.mi.lines)):
            for idx2 in range(len(self.mi.lines[idx])):
                if(self.mi.lines[idx][idx2] == 'P'):
                    startx = idx2
                    starty = idx
        return (startx,starty)

    def findEnd(self):
        for idx in range(len(self.mi.lines)):
            for idx2 in range(len(self.mi.lines[idx])):
                if(self.mi.lines[idx][idx2] == '.'):
                    endx = idx2
                    endy = idx
        return (endx,endy)

    def printDiscovered(self):
        for idx in range(self.dims[1]):
            for idx2 in range(self.dims[0]):
                if(self.mi.lines[idx][idx2] == '%'):
                    self.isDiscovered[idx][idx2] = '%'
                print(self.isDiscovered[idx][idx2], end='')
            print()

    def getMyValidNeighbors(self, pos):
        myX = pos[1][0]
        myY = pos[1][1]
        nbs = []
        if(self.mi.lines[myY-1][myX] != '%'):
            nbs.append((self.heur((myX,myY-1),pos[1]),(myX,myY-1)))
        if(self.mi.lines[myY][myX+1] != '%'):
            nbs.append((self.heur((myX+1,myY),pos[1]),(myX+1,myY)))
        if(self.mi.lines[myY+1][myX] != '%'):
            nbs.append((self.heur((myX,myY+1),pos[1]),(myX,myY+1)))
        if(self.mi.lines[myY][myX-1] != '%'):
            nbs.append((self.heur((myX-1,myY),pos[1]),(myX-1,myY)))
        return nbs

    def getMan(self,pos, end):
        sx, sy = pos
        ex, ey = end
        return abs(ex - sx) + abs(ey - sy)

    def heur(self,pos, curr):
        #print(self.getMan(pos, self.endPos))
        #print(self.path_cost[curr[1]][curr[0]] + 1)
        #print()
        return (self.getMan(pos, self.endPos) + self.path_cost[curr[1]][curr[0]] + 1)


    def ASTAR(self):
        self.path = []
        pq = queue.PriorityQueue()
        pq.put((self.heur(self.startPos,self.startPos),(self.startPos)))
        self.path.append(self.startPos)
        next = pq.get()
        while(next[1] != self.endPos):
            if(self.isDiscovered[next[1][1]][next[1][0]] == 0):
                self.isDiscovered[next[1][1]][next[1][0]] = 1
                myNs = self.getMyValidNeighbors(next)
                if(len(myNs) > 0):
                    self.numExpanded+=1
                for n in myNs:
                    if(self.isDiscovered[n[1][1]][n[1][0]] == 0):
                        pq.put(n)
                        self.myParent[n[1][1]][n[1][0]] = next[1]
                        self.path_cost[n[1][1]][n[1][0]] = self.path_cost[next[1][1]][next[1][0]] +1
            next = pq.get()
            self.path.append(next)

    def printSolution(self):
        self.solution = self.isDiscovered
        self.path = []
        currLoc = self.endPos
        self.path.append(currLoc)
        while(currLoc != self.startPos):
            nextLoc = self.myParent[currLoc[1]][currLoc[0]]
            self.path.append(nextLoc)
            currLoc = nextLoc

        for item in self.path:
            self.solution[item[1]][item[0]] = '.'
        for idx in range(self.dims[1]):
            for idx2 in range(self.dims[0]):
                if(self.solution[idx][idx2] == 0 or self.solution[idx][idx2] == 1):
                    self.solution[idx][idx2] = ' '
        self.solution[self.startPos[1]][self.startPos[0]] = 'P'
        for i in range(self.dims[1]):
            for i2 in range(self.dims[0]):
                print(self.solution[i][i2], end='')
            print()

start_time = time.time()
myMazeImp = MazeImporter('mediumMaze.txt')
myASTAR = ASTARFinder(myMazeImp)
myASTAR.ASTAR()
end_time = time.time()
myASTAR.printDiscovered()
print()
myASTAR.printSolution()
print("--- %s seconds ---" % (end_time - start_time))
print("--- %s path cost ---" % (len(myASTAR.path)-1))
print("--- %s nodes expanded ---" % myASTAR.numExpanded)

start_time = time.time()
myMazeImp = MazeImporter('bigMaze.txt')
myASTAR = ASTARFinder(myMazeImp)
myASTAR.ASTAR()
end_time = time.time()
myASTAR.printDiscovered()
print()
myASTAR.printSolution()
print("--- %s seconds ---" % (end_time - start_time))
print("--- %s path cost ---" % (len(myASTAR.path)-1))
print("--- %s nodes expanded ---" % myASTAR.numExpanded)

start_time = time.time()
myMazeImp = MazeImporter('openMaze.txt')
myASTAR = ASTARFinder(myMazeImp)
myASTAR.ASTAR()
end_time = time.time()
myASTAR.printDiscovered()
print()
myASTAR.printSolution()
print("--- %s seconds ---" % (end_time - start_time))
print("--- %s path cost ---" % (len(myASTAR.path)-1))
print("--- %s nodes expanded ---" % myASTAR.numExpanded)
