# MazeSolvers

My solution utilizes Python’s queue and time packages. For BFS_Finder, I use a standard queue in an iterative approach. 
My DFS_Finder uses a standard list as a ‘stack’ by appending and popping as needed in an iterative approach. 
My Greedy Best First Search, GBFS_Finder, uses Python’s Priority Queue to evaluate immediate neighbors and determine next step. 
Finally, my A*, ASTAR_Finder, also uses a Priority Queue sorted by a heuristic defined as the sum of Manhattan Distance from target node to the end goal and the path cost from the start to current node. 
All four of my solutions utilize two-dimensional lists to track each node’s discovery and predecessor in the given traversal. 
My A* solution also uses a two-dimensional list to keep track of path costs for each node.

My heuristic for A* is a sum of path cost and Manhattan Distance. Path cost is calculated as the total cost in steps from the start point to the current node. 
Manhattan Distance calculated here is the right-angles distance from the given node to the end point. My algorithm finds the nearest dot, determines an Manhattan Distance to this dot, 
travels to it using A*, marks it as visited, and then finds the next nearest dot to it. In this way, we find an efficient path from dot to dot and don’t waste steps at the beginning. 
Because I use an A* to find the path to the first dot and between each dot, and because A* is admissible, my solution is admissible.

My Suboptimal search algorithm finds the nearest dot, determines an A* path to this dot, marks it as visited, and then finds the next nearest dot to it. 
This quick algorithm has a path cost of 345, expands 496 nodes, and takes 0.2746s on my computer. 

My Sokoban solver attempts a cursory solution using BFS to find a path from the current state to a solved state. 
I include the Sokoban and Rocks locations as the content for the state variable, and explore the potential following states. 
I did some research on this and found out about Game Trees, which helped me conceptualize about how I could find a solution.