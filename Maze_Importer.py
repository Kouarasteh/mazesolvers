class MazeImporter:
    def __init__(self, mazefile):
        with open(mazefile) as f:
            self.lines = f.readlines()
            for idx in range(len(self.lines)):
                self.lines[idx] = self.lines[idx].replace('\n','')
                self.lines[idx] = list(self.lines[idx])
