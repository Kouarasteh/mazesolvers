from Maze_Importer import MazeImporter
import numpy as np
import queue
class sokoban:
    def __init__(self, mi):
        self.mi = mi
        self.dims = (len(self.mi.lines[0]), len(self.mi.lines))
        self.rockPos = []
        self.holePos = []
        self.wallPos = []
        self.numWalls = self.findWalls()
        self.sPos = self.findS()
        self.numRocks = self.findRocks()
        self.numHoles = self.findHoles()
        self.visitedStates = []
        self.board = [[0 for x in range(self.dims[0])] for y in range(self.dims[1])]


    def isSolved(self, rPos):
        return sorted(rPos) == sorted(self.holePos)

    def isLocked(self, sPos, rPos):
        return len(self.movesPossible(sPos,rPos))==0

    def findS(self):
        for idx in range(self.dims[0]):
            for idx2 in range(self.dims[1]):
                if(self.mi.lines[idx2][idx] == 'P'):
                    return ((idx2,idx))

    def findRocks(self):
        num = 0
        for idx in range(self.dims[0]):
            for idx2 in range(self.dims[1]):
                if(self.mi.lines[idx2][idx] == 'b' or self.mi.lines[idx2][idx] == 'B'):
                    self.rockPos.append((idx2,idx))
                    num+=1
        return num

    def findHoles(self):
        num = 0
        for idx in range(self.dims[0]):
            for idx2 in range(self.dims[1]):
                if(self.mi.lines[idx2][idx] == '.' or self.mi.lines[idx2][idx] == 'B'):
                    self.holePos.append((idx2,idx))
                    num+=1
        return num

    def findWalls(self):
        num = 0
        for idx in range(self.dims[0]):
            for idx2 in range(self.dims[1]):
                if(self.mi.lines[idx2][idx] == '%'):
                    self.wallPos.append((idx2,idx))
                    num +=1
        return num

    def printMaze(self):
        for line in self.mi.lines:
            print(''.join(line))

    def movesPossible(self, sloc, rockloc):
        moves = []
        #0 for up, 1 for right, 2 for down, 3 for left
        currSx, currSy = sloc
        if(((currSx,currSy-1) not in self.wallPos) and ((currSx,currSy-1) in np.ndindex(self.dims))):
            if((currSx,currSy-1) not in rockloc) or (((currSx,currSy-2) not in self.wallPos) and ((currSx,currSy-2) not in rockloc) and ((currSx,currSy-2) in np.ndindex(self.dims))):
                moves.append(0)
        if(((currSx+1,currSy) not in self.wallPos) and ((currSx+1,currSy) in np.ndindex(self.dims))):
            if((currSx+1,currSy) not in rockloc) or (((currSx+2,currSy) not in self.wallPos) and ((currSx+2,currSy) not in rockloc) and ((currSx+2,currSy) in np.ndindex(self.dims))):
                moves.append(1)
        if(((currSx,currSy+1) not in self.wallPos) and ((currSx,currSy+1) in np.ndindex(self.dims))):
            if((currSx,currSy+1) not in rockloc) or (((currSx,currSy+2) not in self.wallPos) and ((currSx,currSy+2) not in rockloc) and ((currSx,currSy+2) in np.ndindex(self.dims))):
                moves.append(2)
        if(((currSx-1,currSy) not in self.wallPos) and ((currSx-1,currSy) in np.ndindex(self.dims))):
            if((currSx-1,currSy) not in rockloc) or (((currSx-2,currSy) not in self.wallPos) and ((currSx-2,currSy) not in rockloc) and ((currSx-2,currSy) in np.ndindex(self.dims))):
                moves.append(3)
        return moves

    def move(self, dir, sloc, rockloc):
        currSx, currSy = sloc
        if(dir ==0):
            if((currSx,currSy-1) in rockloc):
                rockloc.remove((currSx,currSy-1))
                rockloc.insert(0,(currSx,currSy-2))
            sloc = (currSx,currSy-1)
        elif(dir ==2):
            if((currSx,currSy+1) in rockloc):
                rockloc.remove((currSx,currSy+1))
                rockloc.insert(0,(currSx,currSy+2))
            sloc = (currSx,currSy+1)
        elif(dir ==1):
            if((currSx+1,currSy) in rockloc):
                rockloc.remove((currSx+1,currSy))
                rockloc.insert(0,(currSx+2,currSy))
            sloc = (currSx+1,currSy)
        elif(dir ==3):
            if((currSx-1,currSy) in rockloc):
                rockloc.remove((currSx-1,currSy))
                rockloc.insert(0,(currSx-2,currSy))
            sloc = (currSx-1,currSy)
        return ((sloc,rockloc))

    def printSoko(self,currS, currR):
        for idx in range(self.dims[1]):
            for idx2 in range(self.dims[0]):
                if((idx,idx2) == currS):
                    print('P', end='')
                elif((idx,idx2) in currR):
                    print('b', end='')
                elif((idx,idx2) in self.wallPos):
                    print('%', end='')
                elif((idx,idx2) in self.holePos):
                    print('.', end='')
                elif((idx,idx2) in currR and (idx,idx2) in self.holePos):
                    print('B', end='')
                else:
                    print(' ', end='')
            print()


    def SokoBFS(self):
        q = queue.Queue()
        q.put((self.sPos,self.rockPos))
        while(not q.empty()):
            (currS, currR) = q.get()
            self.printSoko(currS,currR)
            print(currS, currR)
            if(self.isSolved(currR)):
                print('solved!')
            if(self.isLocked(currS,currR)):
                print('locked!')
            if((currS,currR) not in self.visitedStates):
                self.visitedStates.append((currS,currR))
                moves = self.movesPossible(currS,currR)
                for dir in moves:
                    newS = currS
                    newR = currR
                    (newS,newR) = self.move(dir, newS, newR)
                    q.put((newS,newR))



m = MazeImporter('sokoban1.txt')
s = sokoban(m)
s.printMaze()
print(s.rockPos)
print(s.holePos)
print(s.sPos)
print(s.wallPos)
s.SokoBFS()
