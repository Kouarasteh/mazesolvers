from Maze_Importer import MazeImporter
import time
import queue
class multiASTARFinder:
    def __init__(self, mi):
        self.mi = mi
        self.startPos = self.findStart()
        self.DotPos = self.findDots()
        self.dims = (len(self.mi.lines[0]), len(self.mi.lines))
        self.isDiscovered = [[0 for x in range(self.dims[0])] for y in range(self.dims[1])]
        self.myParent = [[(-1,-1) for x in range(self.dims[0])] for y in range(self.dims[1])]
        self.numExpanded = 0
        self.path_cost = [[(0) for x in range(self.dims[0])] for y in range(self.dims[1])]
        self.bigPath = []
        self.orderedDots = []
    def printMaze(self):
        for line in self.mi.lines:
            print(''.join(line))

    def findStart(self):
        for idx in range(len(self.mi.lines)):
            for idx2 in range(len(self.mi.lines[idx])):
                if(self.mi.lines[idx][idx2] == 'P'):
                    startx = idx2
                    starty = idx
        return (startx,starty)

    def findDots(self):
        dots = []
        for idx in range(len(self.mi.lines)):
            for idx2 in range(len(self.mi.lines[idx])):
                if(self.mi.lines[idx][idx2] == '.'):
                    endx = idx2
                    endy = idx
                    dots.append((endx,endy))
        return dots

    def printDiscovered(self):
        for idx in range(self.dims[1]):
            for idx2 in range(self.dims[0]):
                if(self.mi.lines[idx][idx2] == '%'):
                    self.isDiscovered[idx][idx2] = '%'
                print(self.isDiscovered[idx][idx2], end='')
            print()

    def getMyValidNeighbors(self, pos, end):
        myX = pos[0]
        myY = pos[1]
        nbs = []
        if(self.mi.lines[myY-1][myX] != '%'):
            nbs.append((self.heur((myX,myY-1),(pos[0], pos[1]), end),(myX,myY-1)))
        if(self.mi.lines[myY][myX+1] != '%'):
            nbs.append((self.heur((myX+1,myY),(pos[0], pos[1]), end),(myX+1,myY)))
        if(self.mi.lines[myY+1][myX] != '%'):
            nbs.append((self.heur((myX,myY+1),(pos[0], pos[1]), end),(myX,myY+1)))
        if(self.mi.lines[myY][myX-1] != '%'):
            nbs.append((self.heur((myX-1,myY),(pos[0], pos[1]), end),(myX-1,myY)))
        return nbs

    def getMan(self,pos, end):
        sx, sy = pos
        ex, ey = end
        return abs(ex - sx) + abs(ey - sy)

    def heur(self,pos, curr, end):
        #print(self.getMan(pos, self.endPos))
        #print(self.path_cost[curr[1]][curr[0]] + 1)
        #print()
        return (self.getMan(pos, end) + self.path_cost[curr[1]][curr[0]] + 1)

    def getNearestDot(self, st):
        paths = queue.PriorityQueue()
        chosenPath = []
        for dot in self.DotPos:
            if(self.isDiscovered[dot[1]][dot[0]] == 0):
                myPath = self.ASTAR(st, dot)
                paths.put((len(myPath), myPath))
        chosenPath = paths.get()[1]
        targ = (chosenPath[len(chosenPath)-1][0],chosenPath[len(chosenPath)-1][1])
        self.orderedDots.append((targ[1],targ[0]))
        self.isDiscovered[targ[1]][targ[0]] = 1
        cur = (chosenPath[len(chosenPath)-1][0],chosenPath[len(chosenPath)-1][1])
        pathchunk = []
        while(cur != st):
            pathchunk.append(self.myParent[cur[1]][cur[0]])
            cur = self.myParent[cur[1]][cur[0]]
        pathchunk.reverse()
        for item in pathchunk:
            self.bigPath.append(item)
        return targ

    def topLevel(self):
        nextStart = self.startPos
        for dot in self.DotPos:
            temp = self.getNearestDot(nextStart)
            nextStart = temp


    def ASTAR(self, start, end):
        localIsDiscovered = [[0 for x in range(self.dims[0])] for y in range(self.dims[1])]
        path = []
        pq = queue.PriorityQueue()
        pq.put((self.heur(start,start,end),start))
        path.append(start)
        next = pq.get()
        while(next[1] != end):
            if(localIsDiscovered[next[1][1]][next[1][0]] == 0):
                localIsDiscovered[next[1][1]][next[1][0]] = 1
                myNs = self.getMyValidNeighbors(next[1], end)
                if(len(myNs) > 0):
                    self.numExpanded+=1
                for n in myNs:
                    if(localIsDiscovered[n[1][1]][n[1][0]] == 0):
                        pq.put(n)
                        self.myParent[n[1][1]][n[1][0]] = next[1]
                        self.path_cost[n[1][1]][n[1][0]] = self.path_cost[next[1][1]][next[1][0]] +1
            next = pq.get()
            path.append(next[1])
        return path

    def printSolution(self, endPos):
        self.solution = self.isDiscovered
        for item in self.bigPath:
            self.solution[item[1]][item[0]] = '.'
        for idx in range(len(self.orderedDots)):
            print(self.orderedDots[idx])
            dotx,doty = self.orderedDots[idx]
            self.solution[dotx][doty] = (idx+1)
        for idx in range(self.dims[1]):
            for idx2 in range(self.dims[0]):
                if(self.solution[idx][idx2] == 0 or self.solution[idx][idx2] == 1):
                    self.solution[idx][idx2] = ' '
        self.solution[self.startPos[1]][self.startPos[0]] = 'P'
        for i in range(self.dims[1]):
            for i2 in range(self.dims[0]):
                print(self.solution[i][i2], end='')
            print()

start_time = time.time()
myMazeImp = MazeImporter('tinysearch.txt')
myASTAR = multiASTARFinder(myMazeImp)
myASTAR.topLevel()
end_time = time.time()
myASTAR.printDiscovered()
print()
print(len(myASTAR.bigPath))
print(myASTAR.bigPath)
myASTAR.printSolution(myASTAR.bigPath[(len(myASTAR.bigPath) -1)])
print("--- %s seconds ---" % (end_time - start_time))
print("--- %s path cost ---" % (len(myASTAR.bigPath)-1))
print("--- %s nodes expanded ---" % myASTAR.numExpanded)
start_time = time.time()
myMazeImp = MazeImporter('smallsearch.txt')
myASTAR = multiASTARFinder(myMazeImp)
myASTAR.topLevel()
end_time = time.time()
myASTAR.printDiscovered()
print()
print(len(myASTAR.bigPath))
print(myASTAR.bigPath)
myASTAR.printSolution(myASTAR.bigPath[(len(myASTAR.bigPath) -1)])
print("--- %s seconds ---" % (end_time - start_time))
print("--- %s path cost ---" % (len(myASTAR.bigPath)-1))
print("--- %s nodes expanded ---" % myASTAR.numExpanded)
start_time = time.time()
myMazeImp = MazeImporter('mediumsearch.txt')
myASTAR = multiASTARFinder(myMazeImp)
myASTAR.topLevel()
end_time = time.time()
myASTAR.printDiscovered()
print()
print(len(myASTAR.bigPath))
print(myASTAR.bigPath)
myASTAR.printSolution(myASTAR.bigPath[(len(myASTAR.bigPath) -1)])
print("--- %s seconds ---" % (end_time - start_time))
print("--- %s path cost ---" % (len(myASTAR.bigPath)-1))
print("--- %s nodes expanded ---" % myASTAR.numExpanded)
